import styled from "styled-components";
import { Button, Menu } from "antd";

export const Container = styled.div`
  width: 100vw;
  height: calc(100vh - 50px);
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const LoginBox = styled.div`
  background: white;
  background-color: #ffffff;
  margin-top: 20px;
  padding: 30px 50px;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  justify-items: center;
  align-content: center;
  box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.109);
  border-radius: 10px;
`;

export const RegisterBox = styled(LoginBox)`
  width: 500px
`;

export const StyledButton = styled(Button)`
  margin-top: 5px;
  width: 100%;
`;

export const StyledMenu = styled(Menu)`
  font-size: 1.1rem;
  display: flex;
  justify-content: flex-end;
  padding-right: 20px;
`;

export const Unauthorized = styled.p`
  color: red;
  text-align: center;
`;

export const MessageSucess = styled.p`
  color: green;
  text-align: center;
`;

export const Title = styled.div`
  text-align: center;
  font-size: 2rem;
  color: #001529;
`;
