import React from "react";
import { Link, useLocation } from "react-router-dom";
import { StyledMenu } from "../styled-components";
import { UserAddOutlined } from "@ant-design/icons";

import "antd/dist/antd.css";

const MenuLoggedOff = () => {
  const location = useLocation();

  return (
    <StyledMenu mode="horizontal" selectedKeys={[location.pathname]}>
      <StyledMenu.Item key="/register">
        <Link to="/register"> <UserAddOutlined /> Cadastrar Usuário </Link>
      </StyledMenu.Item>
      <StyledMenu.Item key="/">
        <Link to="/"> Fazer Login </Link>
      </StyledMenu.Item>
    </StyledMenu>
  );
};

export default MenuLoggedOff;
