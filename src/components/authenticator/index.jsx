import React, { useEffect, useState } from "react";
import { Route, Switch, useHistory } from "react-router-dom";
import axios from "axios";

import Login from "../../pages/login-form";
import FeedbackForm from "../../pages/feedback-form/new-feedback";
import Register from "../../pages/register-form";
import Users from "../../pages/users";
import MenuLoggedIn from "../menu-logged-in";
import MenuLoggedOff from "../menu-logged-off";
import Feedbacks from "../../pages/feedback-form/feedbacks";

const Authenticator = () => {
  const [isAuthenticated, setAuthentication] = useState(undefined);
  const history = useHistory();

  useEffect(() => {
    const token = localStorage.getItem("token");
    const toGetApi = "https://ka-users-api.herokuapp.com/users";

    if (!token) {
      setAuthentication(false);
    }

    axios
      .get(toGetApi, {
        headers: { Authorization: token },
      })
      .then((res) => {
        setAuthentication(true);
        history.push("/users");
      })
      .catch(() => {
        setAuthentication(false);
      });
  }, [history, setAuthentication]);

  if (isAuthenticated === undefined) {
    return <div> Loading...</div>;
  }

  if (isAuthenticated === false) {
    return (
      <>
        {!isAuthenticated && <MenuLoggedOff />}
        <Switch>
          <Route exact path="/">
            <Login setAuthentication={setAuthentication} />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
        </Switch>
      </>
    );
  }

  return (
    <>
      {isAuthenticated && (
        <MenuLoggedIn setAuthentication={setAuthentication} />
      )}
      <Switch>
        <Route exact path="/users">
          <Users />
        </Route>
        <Route path="/users/feedbacks/:id">
          <Feedbacks />
        </Route>
        <Route path="/users/:id/new-feedback">
          <FeedbackForm />
        </Route>
      </Switch>
    </>
  );
};

export default Authenticator;
