import React from "react";
import { Link, useLocation } from "react-router-dom";
import { StyledMenu } from "../styled-components";
import { PoweroffOutlined, UserOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";

const MenuLoggedIn = ({ setAuthentication }) => {
  const location = useLocation();

  return (
    <StyledMenu mode="horizontal" selectedKeys={[location.pathname]}>
      <StyledMenu.Item key="/users">
        <Link to="/users">
          <UserOutlined /> Usuários
        </Link>
      </StyledMenu.Item>
      <StyledMenu.Item key="/">
        <Link
          onClick={() => {
            setAuthentication(false);
            localStorage.removeItem("token");
          }}
          to="/"
        >
          <PoweroffOutlined /> Logout
        </Link>
      </StyledMenu.Item>
    </StyledMenu>
  );
};

export default MenuLoggedIn;
