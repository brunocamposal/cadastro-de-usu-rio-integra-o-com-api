import React from "react";
import { Button } from "antd";
import { Link } from "react-router-dom";

export const userHeader = [
    {
      title: "#Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "User",
      dataIndex: "user",
      key: "user",
    },
    {
      title: "E-mail",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Feedbacks",
      key: "feedback",
      render: (text) => (
        <Button > <Link to={`users/feedbacks/${text.id}`}>Visualizar</Link> </Button> 
      ),
    },
  ];