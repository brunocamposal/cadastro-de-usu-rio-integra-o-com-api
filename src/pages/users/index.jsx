import React, { useEffect, useState } from "react";
import axios from "axios";
import { Table } from "antd";
import { userHeader } from "./helper";

import "./users.css";

const Users = () => {
  const token = localStorage.getItem("token");
  const url = "https://ka-users-api.herokuapp.com/users";
  const [userList, setUserList] = useState([]);

  useEffect(() => {
    axios
      .get(url, {
        headers: { Authorization: token },
      })
      .then(({ data }) => {
        setUserList([...userList, ...data]);
      })
      .catch(() => {
        console.log("Erro ao carregar a tabela dos usuários");
      });
  }, []);

  return (
    <div className="main">
      <Table dataSource={userList} columns={userHeader} className="table" />
    </div>
  );
};

export default Users;
