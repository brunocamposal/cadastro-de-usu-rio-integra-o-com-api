import React, { useState } from "react";
import axios from "axios";
import { Form, Input } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import {
  Container,
  LoginBox,
  StyledButton,
  Unauthorized,
  Title,
} from "../../components/styled-components";

const LoginForm = ({ setAuthentication }) => {
  const history = useHistory();
  const [invalidPwd, setErrorMessage] = useState("");

  const logingIn = (data) => {
    const toAuthenticate = "https://ka-users-api.herokuapp.com/authenticate";

    axios
      .post(toAuthenticate, { ...data })
      .then((res) => {
        localStorage.setItem("token", res.data.auth_token);
        setAuthentication(true);
        history.push("/users");
      })
      .catch((err) => {
        if (err.response.status === 401) {
          return setErrorMessage("Usuário/Senha incorreto.");
        }

        setErrorMessage("Algo deu errado! Tente novamente.");
      });
  };

  return (
    <Container>
      <LoginBox>
        <Form name="normal_login" className="login-form" onFinish={logingIn}>
          <Form.Item>
            <Title>Login</Title>
          </Form.Item>
          <Form.Item
            name="user"  
            rules={[{ required: true, message: "Usuário obrigatório" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={"Usuário"}
            />
          </Form.Item>

          <Form.Item
            name="password"
            rules={[{ required: true, message: "Senha obrigatória" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={"Senha"}
            />
          </Form.Item>

          <Form.Item>
            <StyledButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Entrar
            </StyledButton>
          </Form.Item>
          {invalidPwd && <Unauthorized>{invalidPwd}</Unauthorized>}
        </Form>
      </LoginBox>
    </Container>
  );
};

export default LoginForm;
