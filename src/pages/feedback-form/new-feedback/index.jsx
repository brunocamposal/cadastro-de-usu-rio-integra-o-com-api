import React, { useState } from "react";
import { Form, Input, InputNumber } from "antd";
import styled from "styled-components";
import { useParams, useHistory} from "react-router-dom";
import {
  Container,
  LoginBox,
  StyledButton,
  MessageSucess,
  Title,
} from "../../../components/styled-components";
import axios from "axios";

const FeedbackForm = () => {
  const { id } = useParams();
  const history = useHistory();
  const [res, setRes] = useState("");

  const sendFeedback = (data) => {
    const postUrl = `https://ka-users-api.herokuapp.com/users/${id}/feedbacks`;
    const token = localStorage.getItem("token");

    axios
      .post(
        postUrl,
        {
          feedback: data,
        },
        {
          headers: { Authorization: token },
        }
      )
      .then(() => {
        setRes("Feedback cadastrado com sucesso!");
        history.push(`/users/feedbacks/${id}`);
      })
      .catch((err) => err.response);
  };

  return (
    <Container>
      <FeedbackBox>
        <Form className="Item">
          <FeedbackTitle>New Feedback</FeedbackTitle>
        </Form>
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 32 }}
          layout="horizontal"
          initialValues={{ size: "large" }}
          onFinish={sendFeedback}
        >
          <Form.Item
            name="name"
            label="Nome"
            rules={[{ required: true, message: "Insira seu nome" }]}
          >
            <Input placeholder={"Nome"} />
          </Form.Item>

          <Form.Item
            name="comment"
            label="Comentário"
            rules={[{ required: true, message: "Insira algum comentário" }]}
          >
            <Input placeholder={"Comentário"} />
          </Form.Item>

          <Form.Item
            name="grade"
            label="Nota"
            rules={[{ required: true, message: "Insira sua nota" }]}
          >
            <InputNumber />
          </Form.Item>

          <Form.Item>
            <StyledButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Enviar comentário
            </StyledButton>
          </Form.Item>
          <MessageSucess>{res}</MessageSucess>
        </Form>
      </FeedbackBox>
    </Container>
  );
};

export default FeedbackForm;

const FeedbackBox = styled(LoginBox)`
  width: 400px;
`;

const FeedbackTitle = styled(Title)`
  margin: 20px 0;
`;
