export const feedbackHeader = [
    {
      title: "#Id",
      dataIndex: "id",
      key: "id",
    },
    {
      title: "Nome",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Comentários",
      dataIndex: "comment",
      key: "comment",
    },
    {
      title: "Grades",
      dataIndex: "grade",
      key: "grade",
    },
  ];