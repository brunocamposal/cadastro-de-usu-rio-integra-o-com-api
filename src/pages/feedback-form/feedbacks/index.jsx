import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";
import { Table, Button } from "antd";
import { feedbackHeader } from "./helper";

const Feedbacks = () => {
  const [feedbackList, setFeebackList] = useState([]);
  const { id } = useParams();
  const token = localStorage.getItem("token");
  const url = `https://ka-users-api.herokuapp.com/users/${id}/feedbacks `;

  useEffect(() => {
    axios
      .get(url, {
        headers: { Authorization: token },
      })
      .then(({ data }) => {
        setFeebackList([...feedbackList, ...data]);
      })
      .catch(() => {
        console.log("erro");
      });
  }, []);

  
  return (
    <div className="main">
      <Button className="button">
        <Link to={`/users/${id}/new-feedback`}> New Feedback</Link>
      </Button>
      <Table
        dataSource={feedbackList}
        columns={feedbackHeader}
        className="table"
      />
    </div>
  );
};

export default Feedbacks;
