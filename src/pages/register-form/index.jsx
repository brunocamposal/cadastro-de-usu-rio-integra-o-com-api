import React, { useEffect, useState } from "react";
import { Form, Input } from "antd";
import axios from "axios";
import { useHistory } from "react-router-dom";

import {
  Container,
  RegisterBox,
  StyledButton,
  Unauthorized,
  Title,
} from "../../components/styled-components";

const RegisterForm = () => {
  const history = useHistory();
  const [errorMessage, setErrorMessage] = useState("");

  const registerUser = (data) => {
    axios
      .post("https://ka-users-api.herokuapp.com/users", { user: { ...data } })
      .then(() => {
        history.push("/");
      })
      .catch((err) => {
        if (err.response.status === 422) {
          return setErrorMessage("Usuário/Senha já cadastrados.");
        }
        if (err.response.status === 500) {
          return setErrorMessage("Erro no servidor");
        }
      });
  };

  return (
    <Container>
      <RegisterBox>
        <Form
          name="register"
          onFinish={registerUser}
          layout="vertical"
        >
          <Form.Item>
            <Title>Registrar-se</Title>
          </Form.Item>
          <Form.Item
            name="user"
            label="Usuário"
            rules={[
              { required: true, message: "Usuário obrigatório" },
              {
                min: 6,
                message: "Nome do usuário deve conter no mínimio 6 caracteres",
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="name"
            label="Nome Completo"
            rules={[
              { required: true, message: "Nome completo é obrigatório" },
              {
                pattern: /^[a-zA-Z]{4,}(?: [a-zA-Z]+){1,}$/,
                message: "Nome com formato inválido",
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              { required: true, message: "E-mail é obrigatório" },
              {
                pattern: /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/,
                message: "E-mail com formato inválido",
              },
            ]}
            hasFeedback
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            label="Senha"
            rules={[
              { required: true, message: "Senha é obrigatório" },
              {
                pattern: /(?=.*[\!@#$%^&*/?])(?=(.*[a-zA-Z0-9]))(?=(.*)).{1,}/,
                message:
                  "Senha com formato inválido (necessário caracter especial !@#$%^&*",
              },
              { min: 6, message: "Senha deve conter no mínimio 6 caracteres" },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirme a senha"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Confirmar senha é obrigatório",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject("As duas senhas não são iguais");
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item>
            <StyledButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Cadastrar
            </StyledButton>
          </Form.Item>
          <Unauthorized> {errorMessage} </Unauthorized>
        </Form>
      </RegisterBox>
    </Container>
  );
};

export default RegisterForm;
