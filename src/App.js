import React from "react";
import "antd/dist/antd.css";

import Authenticator from "./components/authenticator";

const App = () => {
  return (
    <div>
      <Authenticator />
    </div>
  );
};

export default App;
